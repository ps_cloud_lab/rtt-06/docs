﻿Answers:
1. Action recommended.
2. There is unrestricted access through this security group through the ports 22 and 8080.
3. We are working with security groups ,webserverSG in us-east-1 region and DatabaseServerSG in us-west-region-2, Having tcp ports 22 and 8080 respectively.
4. A security group  rule should have a source IP with a/0 suffix only for ports 25,80 or 443.
5. Restrict access to only those IP addresses that require it.
![image](images/Gi.jpg)

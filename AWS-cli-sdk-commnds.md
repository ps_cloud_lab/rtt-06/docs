```
# Find AWS Account number and userId
aws sts get-caller-identity

# See SSM patch groups
aws ssm describe-patch-groups

# after you make your baselines, you can set this variable with their id.
baselineid=pb-0b8cc781e3d3fdb7b
aws ssm register-patch-baseline-for-patch-group \
--baseline-id $(echo $baselineid| tr -d '"') \
--patch-group "LinuxProd"

baselineid=pb-04cffc2ba6b9982b6
aws ssm register-patch-baseline-for-patch-group \
--baseline-id $(echo $baselineid| tr -d '"') \
--patch-group "WindowsProd"

aws ssm describe-patch-groups

# get your policy arn
aws iam list-attached-user-policies \
--user-name user_name

# get your version id
aws iam get-policy \
--policy-arn policy_arn

# Filter all ec2 ami's using aws cli version 2:
aws ec2 describe-instances \
--filters "Name=instance-state-name,Values=running" \
--query "Reservations[*].Instances[*].ImageId" \
--output text
```

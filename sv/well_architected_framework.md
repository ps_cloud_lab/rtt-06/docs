### AWS Well Architected Framework
   The AWS Well-Architected Framework helps you understand the pros and cons of decisions you make while building systems on AWS. By using the Framework you will learn architectural best practices for designing and operating reliable, secure, efficient, cost-effective, and sustainable systems in the cloud.

## Pillers of Framework
## 1. Operational Excellence pillar
   - The ability to run and moniter system deliver business value
   - continually improve supportingprocessrs and procedures
   # Design Principle
   - Perform operations as code
   - Annotate documentation
   - Make frequent, small, reversible changes
   - Refine operations procedures frequently
   - Anticipate failure
   - Learn from all operational failures
   # Best Practise
   Prepare: AWS Config and AWS Config rules can be used to create standards for workloads and to determine if environments are compliant with those standards before being put into production.

   Operate : Amazon CloudWatch allows you to monitor the operational health of a workload.

   Evolve : Amazon Elasticsearch Service (Amazon ES) allows you to analyze your log

data to gain actionable insights quickly and securely.

## 2. Security pillar
   - The ability to protect information ,systems and assets while    delivery  business 
   - value through risk assessments and mitigation strategies

  # Design Principles
   - Implement a strong identity foundation
   - Enable traceability
   - Apply security at all layers:
   - Automate security best practices
   - Protect data in transit and at rest
   -  Keep people away from data
   - Prepare for security events

  # Best Practices
  1. Identity and Access Management
  2. Detective Controls
  3. Infrastructure Protection
  4. Data Protection
  5. Incident Response

## 3. Reliability piller
   - The ability to recover from failures 
   # Design Principles
   - Test recovery procedures
   - Automatically recover from failure:
   - Scale horizontally to increase aggregate system availability
   - Stop guessing capacity
   - Manage change in automation
   
   # Best Practices
   1. Foundations
   2. Change Management
   3 Failure Management
 
## 4. Performance Efficiency pillar
   - The ability to use  computing resources efficiently to meet system requirements
   -  Maintain that efficiency as demand changes and technologies evolve
  # Design Principles
   - Democratize advanced technologies
   - Go global in minutes
   - Use serverless architectures
   - Experiment more often:
   - Mechanical sympathy

   # Best Practices
   1. Selection
   2. Review
   3. Monitoring
   4. Trade-offs
   
## 5. Cost Optimization Pillar
   - The ability to achieve business outcomes at l;owest price point
  # Design Principles
  - Adopt a consumption model
  - Measure overall efficiency
  - Stop spending money on data center operations
  - Analyze and attribute expenditure
  - Use managed and application level services to reduce cost of ownership

  # Best Practices

  1. Cost-Effective Resources
  2. Matching supply and demand
  3. Expenditure Awareness
  4. Optimizing Over Time 



# What is an API?

APIs (Application programming interfaces) are contracts that allow code to talk to other code, building blocks to modern software

Web APIs work over the internet. Weather apps and flight apps that have data already entered.
Transforms click and keyboard strokes to queries within the server and database
Instead of data returned as HTML, API usualy respond in XML or JSON
API client used to show same information in different ways
Both the client and the server need to agree on the structure and representation of data
APIs help developers integrate exciting features and build automations without reinventing the wheel
ex: using a Weather API instead of launching your own weather balloons

APIs allow enterprises to open up their product for faster innovation
ex: apps that interact with Twitter or Meta APIs by posting on your behalf or reading tweets

APIs can be products themselves
ex: Software as a Service (SaaS) products like Stripe's payment APIs or Twilio's text messaging and email APIs
Customer = Client (typically a browser, web app or mobile app)
Waiter = API (interface for interacting with the backend)
Kitchen = Server (backend where the processing happens)

## Types of APIs
A hardware API is an interface for software to talk to hardware.
Ex: How your phone's camera talks to the operating system.

A software library API is an interface for directly consuming code from another code base.
Ex: Using methods from a library you import into your application, or using an image processing library to apply a filter to your image.

A web API is an interface for communicating across code bases over a network.
Ex: Fetching current stock prices from a finance API over the internet, or sending your image to Instagram's servers

Multiple API types may be used to achieve a simple task. For example, uploading a photo to Instagram makes use of various APIs:

Request methods
When we make an HTTP call to a server, we specify a request method that indicates the type of operation we are about to perform. These are also called HTTP verbs.

Here are some common HTTP request methods that correspond to the CRUD operations mentioned earlier. You can see a list of more methods here:

* GET	Retrieve data (Read)
* POST	Send data (Create)
* PUT/PATCH	Update data (Update)
* PUT usually replaces an entire resource, whereas PATCH usually is for partial updates
DELETE	Delete data (Delete)

## Requests
![Request graphic](https://everpath-course-content.s3-accelerate.amazonaws.com/instructor%2F26fp2261340y1ukokimvca8su%2Fpublic%2F1649285301%2Frequest+response+pattern.1649285301835.png)
The client is the agent making a request. A client could be a browser or an application you have coded, for example. In our case Postman is the client because that's how we sent the request. 

The request is sent over a network to some server.
Types of responses/errors:
* Success (2xx)
* Redirection(3xx) 
* Client Error(4xx) 
* Server Error(5xx)

## Query parameter syntax
Query parameters are added to the end of the path. They start with a question mark (?), followed by the key value pairs in the format: (key>=<value)
For example, this request might fetch all photos that have landscape orientation:

GET https://some-api.com/photos?orientation=landscape

If there are multiple query parameters, each is separated by an ampersand (&). Below, two query parameters to specify the orientation and size of photos to be returned:

GET https://some-api.com/photos?orientation=landscape&size=500x400

# SOAP V. REST APIs
In the world of software development, APIs (Application Programming Interfaces) play a crucial role in enabling different software applications to communicate and exchange data with each other. Two commonly used types of APIs are SOAP (Simple Object Access Protocol) and REST (Representational State Transfer) APIs. While they may seem similar at first glance, there are significant differences between the two. In this article, we will explore the similarities and differences between SOAP and REST APIs, as well as their use cases.

Similarities between SOAP and REST APIs

Both SOAP and REST APIs are designed to allow different software applications to communicate with each other and exchange data. Both APIs use HTTP as a transport protocol and can be used to exchange data in various formats, including XML and JSON. Additionally, both APIs require a set of rules to be followed to ensure successful communication between the client and the server.

Differences between SOAP and REST APIs

SOAP and REST APIs differ in several key areas, including:

Message format: SOAP APIs use XML for their message format, while REST APIs can use various formats, including JSON, XML, and HTML.

Performance: SOAP APIs are generally considered to be slower and more heavyweight than REST APIs, which are lightweight and designed for performance.

Security: SOAP APIs provide more security features, including built-in encryption and digital signatures, while REST APIs rely on the underlying transport protocol (i.e., HTTPS) for security.

Use cases for SOAP and REST APIs

SOAP APIs are typically used in enterprise-level applications where security and reliability are critical, such as in the financial or healthcare industries. They are also commonly used in applications that require more complex data structures and where a high degree of interoperability is needed.

REST APIs, on the other hand, are used in a wide range of applications, from simple mobile applications to complex web applications. They are particularly well-suited for applications that require high performance and scalability, such as social media platforms, e-commerce websites, and IoT (Internet of Things) devices.

In conclusion, both SOAP and REST APIs serve important roles in enabling different software applications to communicate and exchange data with each other. While they share some similarities, they also differ in significant ways, including protocol, message format, performance, and security. Understanding these differences is essential when deciding which API to use for a particular application.

More info: Roy Fielding https://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf

# What is Postman?
Postman is a tool for testing, viewing, and building APIs. Previously, to view the outpuf of API requests, we might either do it directly through code, or use a command line tool like curl. Both can be inconvenient. 

##
Sign up for a Postman academy account, and a Postman student account (they are separate things).
The link for the course is: 
https://accounts.skilljar.com/accounts/signup/?next=%2Fauth%2Fendpoint%2Flogin%2Fresult[…]l6l4s6qhihn&d=rst126clzdyy&access_code=s-mon-5-pe-sc_sh-su
To use the web client fully, you will need to download a Desktop Agent. The desktop agent is a middleman that will intercept your requests. 
